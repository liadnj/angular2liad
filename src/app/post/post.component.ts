import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
    inputs:['post']
})
export class PostComponent implements OnInit {
@Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  post:Post;
  tempPost:Post = {title:null,content:null,author:null};
  isEdit : boolean = false;
   editButtonText = 'Edit';

    constructor() { }


    sendDelete(){
   this.deleteEvent.emit(this.post);
  }

   cancelEdit(){
    this.isEdit = false;
    this.post.title = this.tempPost.title;
    this.post.content = this.tempPost.content;
    this.post.author = this.tempPost.author;
    this.editButtonText = 'Edit'; 
  }
 

   toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';

      if(this.isEdit){
    this.tempPost.title = this.post.title;
    this.tempPost.content = this.post.content; 
    this.tempPost.author = this.post.author;
     } else {     
       this.editEvent.emit(this.post);
     }
  }
   
 

 
  ngOnInit() {
  }

}
