import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { RouterModule, Routes } from '@angular/router';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';
import{AngularFireModule} from 'angularfire2';
import { FireComponent } from './fire/fire.component';

export const firebaseConfig = {
     apiKey: "AIzaSyC6yed96atac3bGdS_CMO03_sHeM1ibiAA",
    authDomain: "angular2liad.firebaseapp.com",
    databaseURL: "https://angular2liad.firebaseio.com",
    storageBucket: "angular2liad.appspot.com",
    messagingSenderId: "37854101676"
  }


const appRoutes: Routes = [
 { path: 'users', component: UsersComponent },
 { path: 'posts', component: PostsComponent },
 { path: 'fire', component: FireComponent },
 { path: '', component: UsersComponent },
 { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent,
    FireComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
