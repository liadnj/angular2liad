import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2'; 




@Injectable()
export class PostsService {
postsObservable;

constructor(private af:AngularFire) { }
  
  addPost(post){
    this.postsObservable.push(post);
  }

  deletePost(post){
    this.af.database.object('/posts/' + post.$key).remove();
    console.log('/posts/' + post.$key);
  }

  updatePost(post){
    let post1 = {title:post.title}
    console.log(post1);
    this.af.database.object('/posts/' + post.$key).update(post1)
  }  
getPosts(){
    this.postsObservable = this.af.database.list('/posts');
    return this.postsObservable;
	}
 

  

 

}
