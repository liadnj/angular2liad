import { Angular2LiadPage } from './app.po';

describe('angular2-liad App', function() {
  let page: Angular2LiadPage;

  beforeEach(() => {
    page = new Angular2LiadPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
